//
// Created by Alexandr Stegnin on 06.04.2023.
//

#include <iostream>
#include "Animal.h"

using namespace std;

#ifndef SB_CPP_PRACTICE_DOG_H
#define SB_CPP_PRACTICE_DOG_H

#endif //SB_CPP_PRACTICE_DOG_H

class Dog : public Animal
{
    void Voice() override
    {
        cout << "Woof!" << endl;
    }
};