//
// Created by Alexandr Stegnin on 06.04.2023.
//
#include <iostream>
#include <string>
#include "Player.h"

using namespace std;

/**
 * Функция заполняет массив игроков по введённым пользователем данным
 * @param Players массив игроков
 * @param size размер массива
 */
void FillPlayers(Player Players[], const int size)
{
    for (int i = 0; i < size; ++i) {
        string name;
        int score;
        cout << "Введите имя для " << i + 1 << " игрока: ";
        cin >> name;
        cout << "Введите колличество набранных очков для " << i + 1 << " игрока: ";
        cin >> score;
        Players[i].SetName(name);
        Players[i].SetScore(score);
    }
}

/**
 * Функция сортирует игроков по набранным очкам по убыванию
 * @param players массив игроков
 * @param size размер массива
 */
void SortPlayers(Player players[], const int size)
{
    for (int i = 0; i < size; ++i) {
        for (int j = i + 1; j < size; ++j) {
            if (players[i].GetScore() < players[j].GetScore())
            {
                swap(players[i], players[j]);
            }
        }
    }
}

/**
 * Функция печатает информацию об игроке
 * @param player игрок
 */
void PrintPlayerInfo(Player player)
{
    cout << "Player name = " << player.GetName() << ", score = " << player.GetScore() << endl;
}

/**
 * Функция печатает информацию об игроках
 * @param Players массив игроков
 * @param size размер массива
 */
void PrintPlayersInfo(Player Players[], const int size)
{
    for (int i = 0; i < size; ++i)
    {
        PrintPlayerInfo(Players[i]);
    }
}