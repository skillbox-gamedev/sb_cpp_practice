//
// Created by Alexandr Stegnin on 05.04.2023.
//

#ifndef SB_CPP_PRACTICE_VECTOR_H
#define SB_CPP_PRACTICE_VECTOR_H


#include <cmath>

class Vector
{
private:
    double x, y, z;
public:
    Vector(): x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z)
    {
        x = _x;
        y = _y;
        z = _z;
    }

    double GetX()
    {
        return x;
    }

    double GetY()
    {
        return y;
    }

    double GetZ()
    {
        return z;
    }

    void SetX(double _x)
    {
        x = _x;
    }

    void SetY(double _y)
    {
        y = _y;
    }

    void SetZ(double _z)
    {
        z = _z;
    }

    double GetVectorLength()
    {
        return sqrt(x * x + y * y + z * z);
    }
};


#endif //SB_CPP_PRACTICE_VECTOR_H
