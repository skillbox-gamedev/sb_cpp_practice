//
// Created by Alexandr Stegnin on 06.04.2023.
//

#include <iostream>
#include "Animal.h"

using namespace std;

#ifndef SB_CPP_PRACTICE_CAT_H
#define SB_CPP_PRACTICE_CAT_H

#endif //SB_CPP_PRACTICE_CAT_H

class Cat : public Animal
{
    void Voice() override
    {
        cout << "Mur!" << endl;
    }
};