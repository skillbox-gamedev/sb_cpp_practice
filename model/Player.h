//
// Created by Alexandr Stegnin on 05.04.2023.
//

#include <string>
#include <utility>

#ifndef SB_CPP_PRACTICE_PLAYER_H
#define SB_CPP_PRACTICE_PLAYER_H

#endif //SB_CPP_PRACTICE_PLAYER_H

class Player {
private:
    std::string name;
    int score;
public:
    Player() : score(0) {}

    Player(std::string _name, int _score) {
        name = std::move(_name);
        score = _score;
    }

    std::string GetName()
    {
        return name;
    }

    int GetScore()
    {
        return score;
    }

    void SetName(std::string _newName)
    {
        name = std::move(_newName);
    }
    void SetScore(int _score)
    {
        score = _score;
    }
};

void FillPlayers(Player Players[], int size);

void SortPlayers(Player players[], int size);

void PrintPlayerInfo(Player player);

void PrintPlayersInfo(Player Players[], int size);