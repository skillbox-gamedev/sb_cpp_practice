//
// Created by Alexandr Stegnin on 06.04.2023.
//

#ifndef SB_CPP_PRACTICE_ANIMAL_H
#define SB_CPP_PRACTICE_ANIMAL_H


class Animal
{
public:
    virtual void Voice() = 0;
};


#endif //SB_CPP_PRACTICE_ANIMAL_H
