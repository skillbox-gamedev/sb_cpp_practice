//
// Created by Alexandr Stegnin on 06.04.2023.
//

#include <iostream>
#include "Animal.h"

using namespace std;

#ifndef SB_CPP_PRACTICE_BIRD_H
#define SB_CPP_PRACTICE_BIRD_H

#endif //SB_CPP_PRACTICE_BIRD_H

class Bird : public Animal
{
    void Voice() override
    {
        cout << "Chik-chirik!" << endl;
    }
};