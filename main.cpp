#include <iostream>
#include "model/Animal.h"
#include "model/Dog.h"
#include "model/Cat.h"
#include "model/Bird.h"

using namespace std;

void FillAnimals(Animal* animals[], const int size)
{
    for (int i = 0; i < size; ++i) {
        if ((i % 2) == 0)
        {
            animals[i] = new Dog;
        }
        else if ((i % 3) == 0) {
            animals[i] = new Cat;
        }
        else
        {
            animals[i] = new Bird;
        }
    }
}

int main() {
    const int SIZE = 5;
    auto animals = new Animal*[SIZE];

    FillAnimals(animals, SIZE);

    for (int i = 0; i < SIZE; ++i) {
        animals[i]->Voice();
    }

    delete[] animals;

    return 0;
}


