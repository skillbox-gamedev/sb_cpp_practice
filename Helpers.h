//
// Created by Alexandr Stegnin on 02.04.2023.
//
#include <cmath>
#ifndef SB_CPP_PRACTICE_HELPERS_H
#define SB_CPP_PRACTICE_HELPERS_H

#endif //SB_CPP_PRACTICE_HELPERS_H

/*
 * Функция возвращает сумму переданных параметров,
 * возведённую в квадрат
 */
double MyPow(int a, int b)
{
    return pow((a + b), 2);
}


/*
 * Функция распечатывает информацию о переданной строке:
 * - саму строку
 * - длину строки
 * - первый символ
 * - последний символ
 */
void PrintStringInfo(const std::string& InputText)
{
    std::cout << "String = " << InputText << std::endl;
    std::cout << "String length = " << InputText.length() << std::endl;
    std::cout << "First char = " << InputText.front() << std::endl;
    std::cout << "Last char = " << InputText.back() << std::endl;
}

/**
 * Функция печатает чётные или нечётные числа в диапазоне от 0 до Limit
 * @param Limit граница чисел
 * @param IsOdd признак печати только чётных чисел
 */
void PrintOddOrEvenNumbers(int Limit, bool IsOdd = true)
{
    for (int i = IsOdd; i <= Limit; i = i + 2) {
        std::cout << i << " ";
    }
    std::cout << '\n';
}

/**
 * Функция печатает двумерный массив размера size
 * Выводит в консоль сумму строки массива, которая вычисляется как
 * остаток от деления номера сегодняшнего дня на размер массива
 * @param size размер массива
 */
void Print2DArray(int size) {
    int array[size][size];
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            array[i][j] = i + j;
        }
    }
    for (auto & i : array) {
        for (int j : i) {
            std::cout << j;
        }
        std::cout << '\n';
    }

    time_t t = time(nullptr);
    struct tm buf = *localtime(&t);
    int DayNumber = buf.tm_mday;
    int row = DayNumber % size;
    int sum = 0;
    for (int i = 0; i < size; ++i) {
        sum += array[row][i];
    }
    std::cout << "Sum of elements at row " << row << " = " << sum;
}